def print_board(entries):
    line = "+---+---+---+"
    output = line
    n = 0
    for entry in entries:
        if n % 3 == 0:
            output = output + "\n| "
        else:
            output = output + " | "
        output = output + str(entry)
        if n % 3 == 2:
            output = output + " |\n"
            output = output + line
        n = n + 1
    print(output)
    print()

# Function to print out board and who won the game
def game_over(board, winner):
    print_board(board)
    print("GAME OVER")
    print(winner, "has won")
    exit()

# Function to determine if there is a winner in a row
def is_row_winner(board, row_number):
    row = row_number
    if row == 1 and board[0] == board[1] and board[1] == board[2]:
         return True
    elif row == 2 and board[3] == board[4] and board[4] == board[5]:
        return True
    elif row == 3 and board[6] == board[7] and board[7] == board[8]:
        return True

# Function to determine if there is a winner in a column
def is_column_winner(board, column_number):
    column = column_number
    if column == 1 and board[0] == board[3] and board[3] == board[6]:
        return True
    elif column == 2 and board[1] == board[4] and board[4] == board[7]:
        return True
    elif column == 3 and board[2] == board[5] and board[5] == board[8]:
        return True

# Function to determine if there is a winner in diagonals
def is_diagonal_winner(board, diagonal_number):
    diagonal = diagonal_number
    if diagonal == 1 and board[0] == board[4] and board[4] == board[8]:
        return True
    elif diagonal == 2 and board[2] == board[4] and board[4] == board[6]:
        return True

board = [1, 2, 3, 4, 5, 6, 7, 8, 9]
current_player = "X"

for move_number in range(1, 10):
    print_board(board)
    response = input("Where would " + current_player + " like to move? ")
    space_number = int(response) - 1
    board[space_number] = current_player

    if is_row_winner(board, 1):
        game_over(board, board[0])
    elif is_row_winner(board, 2):
        game_over(board, board[3])
    elif is_row_winner(board, 3):
        game_over(board, board[6])

    elif is_column_winner(board, 1):
        game_over(board, board[0])
    elif is_column_winner(board, 2):
        game_over(board, board[1])
    elif is_column_winner(board, 3):
        game_over(board, board[2])

    elif is_diagonal_winner(board, 1):
        game_over(board, board[0])
    elif is_diagonal_winner(board, 2):
        game_over(board, board[2])

    if current_player == "X":
        current_player = "O"
    else:
        current_player = "X"

print("It's a tie!")
